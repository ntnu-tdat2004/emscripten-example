#ifdef __EMSCRIPTEN__
#include <GL/glfw.h>
#include <emscripten/emscripten.h>
#else
#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif
#include <GLFW/glfw3.h>
#endif

#include <iostream>

using namespace std;

void draw() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();

  glTranslatef(0.0, 0.0, -5.0);

  static float rotationX = 0.0;
  static float rotationY = 0.0;
  static float rotationZ = 0.0;
  glRotatef(rotationX, 1.0, 0.0, 0.0);
  glRotatef(rotationY, 0.0, 1.0, 0.0);
  glRotatef(rotationZ, 0.0, 0.0, 1.0);

  glBegin(GL_QUADS);

  // Front square
  glColor3f(1.0, 0.0, 0.0);
  glVertex3f(-1.0, -1.0, 1.0);
  glColor3f(1.0, 0.0, 0.0);
  glVertex3f(1.0, -1.0, 1.0);
  glColor3f(1.0, 0.0, 0.0);
  glVertex3f(1.0, 1.0, 1.0);
  glColor3f(1.0, 0.0, 0.0);
  glVertex3f(-1.0, 1.0, 1.0);

  // Back square
  glColor3f(0.0, 1.0, 0.0);
  glVertex3f(-1.0, -1.0, -1.0);
  glColor3f(0.0, 1.0, 0.0);
  glVertex3f(-1.0, 1.0, -1.0);
  glColor3f(0.0, 1.0, 0.0);
  glVertex3f(1.0, 1.0, -1.0);
  glColor3f(0.0, 1.0, 0.0);
  glVertex3f(1.0, -1.0, -1.0);

  // Top square
  glColor3f(0.0, 0.0, 1.0);
  glVertex3f(-1.0, 1.0, -1.0);
  glColor3f(0.0, 0.0, 1.0);
  glVertex3f(-1.0, 1.0, 1.0);
  glColor3f(0.0, 0.0, 1.0);
  glVertex3f(1.0, 1.0, 1.0);
  glColor3f(0.0, 0.0, 1.0);
  glVertex3f(1.0, 1.0, -1.0);

  // Bottom square
  glColor3f(1.0, 0.0, 1.0);
  glVertex3f(-1.0, -1.0, -1.0);
  glColor3f(1.0, 0.0, 1.0);
  glVertex3f(1.0, -1.0, -1.0);
  glColor3f(1.0, 0.0, 1.0);
  glVertex3f(1.0, -1.0, 1.0);
  glColor3f(1.0, 0.0, 1.0);
  glVertex3f(-1.0, -1.0, 1.0);

  // Right face
  glColor3f(1.0, 1.0, 0.0);
  glVertex3f(1.0, -1.0, -1.0);
  glColor3f(1.0, 1.0, 0.0);
  glVertex3f(1.0, 1.0, -1.0);
  glColor3f(1.0, 1.0, 0.0);
  glVertex3f(1.0, 1.0, 1.0);
  glColor3f(1.0, 1.0, 0.0);
  glVertex3f(1.0, -1.0, 1.0);

  // Left square
  glColor3f(0.0, 1.0, 1.0);
  glVertex3f(-1.0, -1.0, -1.0);
  glColor3f(0.0, 1.0, 1.0);
  glVertex3f(-1.0, -1.0, 1.0);
  glColor3f(0.0, 1.0, 1.0);
  glVertex3f(-1.0, 1.0, 1.0);
  glColor3f(0.0, 1.0, 1.0);
  glVertex3f(-1.0, 1.0, -1.0);

  glEnd();

  static auto last_time = glfwGetTime();
  auto current_time = glfwGetTime();
  auto delta = current_time - last_time;
  last_time = current_time;

  rotationX += delta * 50.0;
  rotationY += delta * 50.0;
  rotationZ += delta * 50.0;
}

int main(void) {
  if (!glfwInit()) {
    cerr << "Error calling glfwInit()" << endl;
    return 1;
  }

  auto window = glfwCreateWindow(640, 480, "Simple example", nullptr, nullptr);
  if (!window) {
    cerr << "Error calling glfwCreateWindow()" << endl;
    glfwTerminate();
    return 1;
  }
  glfwMakeContextCurrent(window);

  glClearColor(0.9, 0.9, 0.9, 0.0);
  glClearDepth(1.0);
  glDepthFunc(GL_LESS);
  glEnable(GL_DEPTH_TEST);
  glShadeModel(GL_SMOOTH);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluPerspective(45.0, 640.0 / 480.0, 0.1, 100.0);

  glMatrixMode(GL_MODELVIEW);

#ifdef __EMSCRIPTEN__
  emscripten_set_main_loop([] {
    draw();
    glfwSwapBuffers(window);
    glfwPollEvents();
  }, 0, true);
#else
  while (!glfwWindowShouldClose(window)) {
    draw();
    glfwSwapBuffers(window);
    glfwPollEvents();
  }
#endif

  glfwTerminate();
}
