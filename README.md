# Example OpenGL application that can be run both native and in a web browser

## Prerequisites
  * [juCi++](https://github.com/cppit/jucipp)
  * Linux or MacOS
  * Node.js and npm

## Installing dependencies

### Arch Linux based distributions
`sudo pacman -S glfw-x11 emscripten`

### OS X
`brew install glfw emscripten binaryen`

## Downloading
```sh
git clone https://gitlab.com/ntnu-tdat2004/emscripten-example
```

## Compiling and running
### Native
```sh
juci emscripten-example
```

Choose Compile and Run in the Project menu.

### Web Browser using WebAssembly
```sh
cd emscripten-example
npm install

emcc main.cpp -o index.html -s LEGACY_GL_EMULATION=1 -s WASM=1 -std=c++11

node server.js
```

Open http://localhost:3000 in a web browser.
